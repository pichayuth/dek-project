
  /**
   * AddNewCard transaction processor function.
   * @param {org.dek.network.AddNewCard} tx The sample transaction instance.
   * @transaction
   */
  function addNewCard(newCard){
    var factory = getFactory();
    var networkName = "org.dek.network";
    
    var card = factory.newResource(networkName, 'Card', newCard.uuid);
    card.userId = newCard.userId;
    card.issuedCompany = newCard.issuedCompany;
    card.point = newCard.point;
    card.cardNumber = newCard.cardNumber;

    return getAssetRegistry(networkName+'.Card')
    .then(function (asset) {
      return asset.add(card)
    });
  }


  
  /**
   * AddNewCard transaction processor function.
   * @param {org.dek.network.AddNewUser} tx The sample transaction instance.
   * @transaction
   */
  function addNewUser(newUser){
    var factory = getFactory();
    var networkName = "org.dek.network";
    
    var user = factory.newResource(networkName, 'User', newUser.uuid);
    user.citizenId = newUser.citizenId;
    user.firstName = newUser.firstName;
    user.lastName = newUser.lastName;

    return getAssetRegistry(networkName+'.User')
    .then(function (asset) {
      return asset.add(user)
    });
  }


  /**
   * AddNewCard transaction processor function.
   * @param {org.dek.network.TransferPoint} tx The sample transaction instance.
   * @transaction
   */
  function TransferPoint(info){
    var factory = getFactory();
    var networkName = "org.dek.network";
    
    console.log('info: ',info);

    var currentCard;
    var transferCard;
    var currentPoint;
    var transferPoint;
    var allCard = {};
    var fromCompany;
    var toCompany;
    return getAssetRegistry(networkName+'.Card').then(function(registry){
      allCard = registry;
      return allCard.get(info.fromCardId);
    }).then(function(card){
      
      if(parseInt(card.point) < info.fromPoint)
        throw new Error('point not enough to transfer');
      currentCard = card;
      currentPoint = card.point;
      card.point=(parseInt(card.point) - parseInt(info.fromPoint));
      allCard.update(card);
      return allCard.get(info.toCardId);

    }).then(function(card){
      transferCard = card;
      transferPoint = card.point;
      card.point=(parseInt(card.point) + parseInt(info.toPoint));
      allCard.update(card);
      return allCard.update(card);

    }).then(function(card){
      var event = factory.newEvent(networkName, 'CardHistory');
      event.transactionName = "TransferPoint";
      event.userId = info.userId;
      event.oldPoint = parseInt(info.fromPoint);
      event.updatePoint = parseInt(info.toPoint);
      event.updateCardId = info.toCardId;
      event.oldCardId = info.fromCardId;
      event.fromCompany = currentCard.issuedCompany;
      event.toCompany = transferCard.issuedCompany;
      event.oldCard = currentCard;
      event.updateCard =transferCard;
      event.dateTime = new Date();
      emit(event);
    }).catch(function(error){
      throw new Error(error);
    });

    
  }


